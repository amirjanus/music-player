# Music Player

In this project we will create media app for playing music. What is specific about this app is that it cannot play music by itself. Our app plays music by finding other apps that use a MediaBrowserService and connecting to their media session.

Blog post -> https://amirjanus.gitlab.io/blog/2019/07/12/music-player/

Apk -> https://gitlab.com/amirjanus/apps