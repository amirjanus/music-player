package com.example.musicplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.musicplayer.client.MediaAppsAdapter;
import com.example.musicplayer.client.MediaPlayerClient;
import com.example.musicplayer.client.MediaApp;
import com.example.musicplayer.client.PlaybackStateHelper;

import java.util.ArrayList;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements MediaAppsAdapter.OnItemSelectedListener,
                                                               AdapterView.OnItemSelectedListener,
                                                               MediaPlayerClient.SessionCallbacks,
                                                               MediaPlayerClient.SessionAppsFoundListener,
                                                               MediaPlayerClient.BrowserAppsFoundListener {
    
    /**
     * Helper class for printing available playback actions to Logcat.
     */
    private static class AvailableActions {
        // Used to signal if PrintToLogcat function was already executed.
        public static boolean m_done = false;
        
        /**
         * Print available playback actions for current media session to Logcat. This function is
         * only executed once.
         *
         * @param playbackState Playback state for current media session.
         */
        public static void PrintToLogcat( PlaybackStateCompat playbackState ) {
            if ( !m_done ) {
                
                // Create string builder.
                StringBuilder builder = new StringBuilder();
                builder.append( " \n" );
                builder.append( "Current actions available on this session\n" );
                
                // Iterate list of playback action and check if particular action is available in current session.
                for ( Map.Entry<String, Long> entry : PlaybackStateHelper.m_action.entrySet() ) {
                    
                    builder.append( entry.getKey() ).append( ": " );
                    builder.append( PlaybackStateHelper.IsActionSupported( playbackState, entry.getValue() ) ).append( "\n" );
                    
                }
                
                Log.d( TAG, builder.toString() );
                
                // Set to true to prevent this code block executing again.
                m_done = true;
                
            }
        }
    }
    
    // Tag general for printing to Logcat.
    public static final String TAG = "ABC123";
    
    // Buttons for audio player controls.
    private ImageButton m_btnSkipPrevious;
    private ImageButton m_btnSkipNext;
    private ImageButton m_btnPlay;
    private ImageButton m_btnPause;
    private ImageButton m_btnStop;
    private ImageButton m_btnRewind;
    private ImageButton m_btnFastForward;
    
    // TextViews for showing track info.
    private TextView m_textTitle;
    private TextView m_textAlbum;
    private TextView m_textArtist;
    
    // TextView for informing user about available and selected media apps.
    private TextView m_textNoBrowserApps;
    private TextView m_textConnectedApp;
    
    // TextView for informing user if app can access notifications.
    private TextView m_textNotificationAccessMessage;
    // Button to go to notification access screen.
    private Button m_btnRequest;
    
    // Adapter and array list for RecyclerView.
    private MediaAppsAdapter m_adapterServiceApps;
    private ArrayList<MediaApp> m_serviceApps;
    
    // References to MediaPlayerClient and playback controls.
    private MediaPlayerClient m_mediaPlayerClient;
    private MediaControllerCompat.TransportControls m_playbackControls;
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        
        m_serviceApps = new ArrayList<>();
        
        InitViews();
        SetButtonListeners();
        InitMediaPlayerClient();
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        
        UpdatePermissionUi();
    }
    
    /**
     * Initialize View elements from this Activity layout file.
     */
    private void InitViews() {
        // Buttons for audio player controls.
        m_btnSkipPrevious = findViewById( R.id.button_skipPrevious );
        m_btnSkipNext = findViewById( R.id.button_skipNext );
        m_btnPause = findViewById( R.id.button_pause );
        m_btnPlay = findViewById( R.id.button_play );
        m_btnStop = findViewById( R.id.button_stop );
        m_btnRewind = findViewById( R.id.button_rewind );
        m_btnFastForward = findViewById( R.id.button_fastForward );
        
        // Spinner and its adapter for selecting shuffle mode.
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource( this, R.array.shuffle_mode, R.layout.support_simple_spinner_dropdown_item );
        adapter.setDropDownViewResource( R.layout.support_simple_spinner_dropdown_item );
        
        Spinner spinnerShuffle = findViewById( R.id.spinner_shuffleMode );
        spinnerShuffle.setAdapter( adapter );
        spinnerShuffle.setOnItemSelectedListener( this );
        
        // Spinner and its adapter for selecting repeat mode.
        adapter = ArrayAdapter.createFromResource( this, R.array.repeat_mode, R.layout.support_simple_spinner_dropdown_item );
        adapter.setDropDownViewResource( R.layout.support_simple_spinner_dropdown_item );
        
        Spinner spinnerRepeat = findViewById( R.id.spinner_repeatMode );
        spinnerRepeat.setAdapter( adapter );
        spinnerRepeat.setOnItemSelectedListener( this );
        
        // TextViews for showing current track info.
        m_textTitle = findViewById( R.id.text_title );
        m_textAlbum = findViewById( R.id.text_album );
        m_textArtist = findViewById( R.id.text_artist );
        
        // TextView for notification access message.
        m_textNotificationAccessMessage = findViewById( R.id.text_notificationAccessMessage );
        
        // TextViews for list of media apps.
        m_textNoBrowserApps = findViewById( R.id.text_noBrowserApps );
        m_textConnectedApp = findViewById( R.id.text_connectedApp );
        
        // Button for requesting notification access.
        m_btnRequest = findViewById( R.id.button_notificationAccess );
        
        // RecyclerView for media apps list.
        RecyclerView recyclerServiceApps = findViewById( R.id.recycler_serviceApps );
        m_adapterServiceApps = new MediaAppsAdapter( m_serviceApps, this );
        recyclerServiceApps.setAdapter( m_adapterServiceApps );
        recyclerServiceApps.setLayoutManager( new LinearLayoutManager( this ) );
    }
    
    /**
     * Attach click listeners to View elements.
     */
    private void SetButtonListeners() {
        m_btnSkipPrevious.setOnClickListener( ( View v ) -> {
            if ( m_playbackControls != null ) {
                m_playbackControls.skipToPrevious();
            }
        } );
        
        m_btnSkipNext.setOnClickListener( ( View v ) -> {
            if ( m_playbackControls != null ) {
                m_playbackControls.skipToNext();
            }
        } );
        
        m_btnPlay.setOnClickListener( ( View v ) -> {
            if ( m_playbackControls != null ) {
                m_playbackControls.play();
            }
        } );
        
        m_btnPause.setOnClickListener( ( View v ) -> {
            if ( m_playbackControls != null ) {
                m_playbackControls.pause();
            }
        } );
        
        m_btnStop.setOnClickListener( ( View v ) -> {
            if ( m_playbackControls != null ) {
                m_playbackControls.stop();
            }
        } );
        
        m_btnRewind.setOnClickListener( ( View v ) -> {
            if ( m_playbackControls != null ) {
                m_playbackControls.rewind();
            }
        } );
        
        m_btnFastForward.setOnClickListener( ( View v ) -> {
            if ( m_playbackControls != null ) {
                m_playbackControls.fastForward();
            }
        } );
        
        m_btnRequest.setOnClickListener( ( View v ) -> MediaPlayerClient.RequestNotificationAccess( this ) );
    }
    
    /**
     * Initialize MediaPlayerClient.
     */
    private void InitMediaPlayerClient() {
        FragmentManager manager = getSupportFragmentManager();
        
        m_mediaPlayerClient = MediaPlayerClient.GetInstance( manager );
        m_mediaPlayerClient.SetCallbacks( this, this, this );
    }
    
    /**
     * Get MediaApp item which user selected from RecyclerView.
     *
     * @param app Selected item.
     */
    @Override
    public void OnItemSelected( MediaApp app ) {
        Log.d( TAG, "Selected: " + app.m_appName + ", " + app.m_packageName );
        
        m_mediaPlayerClient.ConnectToApp( app );
    }
    
    /**
     * Notify when we are connected to media session.
     *
     * @param playbackControls Playback controls.
     * @param mediaApp         App to whose session we are connected to.
     */
    @Override
    public void OnSessionReady( MediaControllerCompat.TransportControls playbackControls, MediaApp mediaApp ) {
        Log.d( TAG, this.toString() + ":OnSessionReady" );
        
        // Set to false so we can print available actions on the current session to Logcat.
        AvailableActions.m_done = false;
        
        // Get reference to playback controls.
        m_playbackControls = playbackControls;
        
        UpdateMediaAppsUi( mediaApp );
    }
    
    /**
     * Notify when current media session is destroyed.
     */
    @Override
    public void OnSessionDestroyed() {
        Log.d( TAG, this.toString() + ":OnSessionDestroyed" );
        
        UpdateMediaAppsUi( null );
    }
    
    /**
     * Notify when playback state changes.
     *
     * @param playbackState Playback state.
     */
    @Override
    public void OnPlaybackStateChanged( PlaybackStateCompat playbackState ) {
        Log.d( TAG, this.toString() + ":OnPlaybackStateChanged" );
        
        AvailableActions.PrintToLogcat( playbackState );
    }
    
    /**
     * Notify when current track metadata changes.
     *
     * @param metadata Current track metadata.
     */
    @Override
    public void OnMetadataChanged( MediaMetadataCompat metadata ) {
        Log.d( TAG, this.toString() + ":OnMetadataChanged" );
        
        UpdateTrackInfoUi( metadata );
    }
    
    /**
     * Notify when AsyncTask finishes fetching list of Media Browser Service apps.
     *
     * @param serviceApps List with Media Browser Service apps.
     */
    @Override
    public void MediaBrowserAppsFound( ArrayList<MediaApp> serviceApps ) {
        // Create string builder to print list to Logcat.
        StringBuilder builder = new StringBuilder( " \nMedia Service Apps" );
        
        if ( serviceApps.size() == 0 ) {
            
            builder.append( "\nNo Media Browser Service apps found." );
            
        } else {
            
            for ( MediaApp app : serviceApps ) {
                builder.append( app.toString() );
            }
            
        }
        
        Log.d( TAG, builder.toString() );
        
        // Update RecyclerView with the new list of apps.
        m_serviceApps.clear();
        m_serviceApps.addAll( serviceApps );
        
        m_adapterServiceApps.notifyDataSetChanged();
        
        UpdateMediaAppsUi();
    }
    
    /**
     * Notify when AsyncTask finishes fetching list of Media Session apps.
     *
     * @param sessionApps List with Media Session apps.
     */
    @Override
    public void MediaSessionAppsFound( ArrayList<MediaApp> sessionApps ) {
        // Create string builder to print list to Logcat.
        StringBuilder builder = new StringBuilder( " \nMedia Session Apps" );
        
        if ( sessionApps.size() == 0 ) {
            
            builder.append( "\nNo Media Session apps found." );
            
        } else {
            
            for ( MediaApp app : sessionApps ) {
                builder.append( app.toString() );
            }
            
        }
        
        Log.d( TAG, builder.toString() );
    }
    
    /**
     * Called when user selected an item in Spinner widget.
     *
     * @param parent   Spinner which user selected.
     * @param view     Selected item View.
     * @param position Selected item position.
     * @param id       Row id of the selected item.
     */
    @Override
    public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
        switch ( parent.getId() ) {
            case R.id.spinner_shuffleMode:
                
                if ( m_playbackControls != null ) {
                    
                    // Get PlaybackStateCompat shuffle mode constant value and set shuffle mode in controller.
                    int shuffleMode = PlaybackStateHelper.GetShuffleModeValue( ( String ) parent.getItemAtPosition( position ) );
                    m_playbackControls.setShuffleMode( shuffleMode );
                    
                }
                
                break;
            
            case R.id.spinner_repeatMode:
                
                if ( m_playbackControls != null ) {
                    
                    // Get PlaybackStateCompat repeat mode constant value and set repeat mode in controller.
                    int repeatMode = PlaybackStateHelper.GetRepeatModeValue( ( String ) parent.getItemAtPosition( position ) );
                    m_playbackControls.setRepeatMode( repeatMode );
                    
                }
                
                break;
        }
    }
    
    /**
     * Called when spinner selection disappears from view.
     *
     * @param parent Spinner which user selected.
     */
    @Override
    public void onNothingSelected( AdapterView<?> parent ) {}
    
    /**
     * Display message to inform user is notification permission granted or not.
     */
    private void UpdatePermissionUi() {
        if ( MediaPlayerClient.IsNotificationListenerEnabled( this ) ) {
            m_textNotificationAccessMessage.setText( getText( R.string.text_notification_permission_granted ) );
        } else {
            m_textNotificationAccessMessage.setText( getText( R.string.text_no_notification_permission ) );
        }
    }
    
    /**
     * Show or hide TextView message when no media browser apps are found.
     */
    private void UpdateMediaAppsUi() {
        if ( m_serviceApps.size() == 0 ) {
            m_textNoBrowserApps.setVisibility( View.VISIBLE );
        } else {
            m_textNoBrowserApps.setVisibility( View.GONE );
        }
    }
    
    /**
     * Show if we are connected to any media app.
     *
     * @param mediaApp Media app info.
     */
    private void UpdateMediaAppsUi( MediaApp mediaApp ) {
        if ( mediaApp != null ) {
            m_textConnectedApp.setText( getString( R.string.text_connected, mediaApp.m_appName ) );
        } else {
            m_textConnectedApp.setText( R.string.text_not_connected );
        }
    }
    
    /**
     * Display current track info.
     *
     * @param metadata Track metadata.
     */
    private void UpdateTrackInfoUi( MediaMetadataCompat metadata ) {
        String title = metadata.getString( MediaMetadataCompat.METADATA_KEY_TITLE );
        m_textTitle.setText( getString( R.string.label_title, title ) );
        
        String artist = metadata.getString( MediaMetadataCompat.METADATA_KEY_ARTIST );
        m_textArtist.setText( getString( R.string.label_artist, artist ) );
        
        String album = metadata.getString( MediaMetadataCompat.METADATA_KEY_ALBUM );
        m_textAlbum.setText( getString( R.string.label_album, album ) );
    }
}
