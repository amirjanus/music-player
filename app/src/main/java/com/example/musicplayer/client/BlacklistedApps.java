package com.example.musicplayer.client;

/**
 * Class holds list of apps that have MediaBrowserService service but are not made for playing music
 * like, for example Google Play Music.
 */
public class BlacklistedApps {
    
    private static String[][] m_apps = {
            { "Bluetooth Share", "com.android.bluetooth" },
            { "Podcasts", "com.google.android.googlequicksearchbox" },
            { "Google Podcasts", "com.google.android.googlequicksearchbox" },
            { "YouTube", "com.google.android.youtube" }
    };
    
    /**
     * Check if particular app is blacklisted.
     *
     * @param app App to check.
     * @return True if app is blacklisted, false otherwise.
     */
    public static boolean IsBlacklisted( MediaApp app ) {
        for ( String[] appRow : m_apps ) {
            
            // Check if app name and package name are in the list of blacklisted apps.
            if ( app.m_appName.equals( appRow[0] ) && app.m_packageName.equals( appRow[1] ) ) {
                return true;
            }
            
        }
        
        return false;
    }
}
