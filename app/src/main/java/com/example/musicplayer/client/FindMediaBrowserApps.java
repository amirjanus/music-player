package com.example.musicplayer.client;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;

import androidx.media.MediaBrowserServiceCompat;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * AsyncTask to get the list of apps that implement MediaBrowserService service.
 */
public class FindMediaBrowserApps extends AsyncTask<Void, Void, ArrayList<MediaApp>> {
    
    /**
     * Implement this interface to get list of MediaBrowserService apps.
     */
    public interface DoneCallback {
        void FindBrowserAppsDone( ArrayList<MediaApp> apps );
    }
    
    private DoneCallback m_callback;
    private WeakReference<Context> m_context;
    
    public FindMediaBrowserApps( DoneCallback callback, Context context ) {
        m_callback = callback;
        m_context = new WeakReference<>( context );
    }
    
    @Override
    protected ArrayList<MediaApp> doInBackground( Void... voids ) {
        // Create an intent with a MediaBrowserService action.
        Intent mediaBrowserServiceIntent = new Intent( MediaBrowserServiceCompat.SERVICE_INTERFACE );
        
        // Get PackageManager instance.
        PackageManager packageManager = m_context.get().getPackageManager();
        
        // Get all apps that have MediaBrowserService with an intent-filter in their manifest.
        List<ResolveInfo> resolveInfo = packageManager.queryIntentServices( mediaBrowserServiceIntent, PackageManager.GET_RESOLVED_FILTER );
        
        // Create new List for MediaApp objects. If no app implements MediaBrowserService return empty list.
        ArrayList<MediaApp> browserApps = new ArrayList<>();
        
        // Iterate through MediaBrowserService apps list and add them to MediaApp list only if they can play music.
        for ( ResolveInfo info : resolveInfo ) {
            
            MediaApp mediaApp = new MediaApp( info.serviceInfo, packageManager );
            
            if ( !BlacklistedApps.IsBlacklisted( mediaApp ) ) {
                browserApps.add( mediaApp );
            }
            
        }
        
        return browserApps;
    }
    
    @Override
    protected void onPostExecute( ArrayList<MediaApp> browserApps ) {
        // Pass the list of MediaBrowserService apps to class that implements DoneCallback interface.
        m_callback.FindBrowserAppsDone( browserApps );
    }
}
