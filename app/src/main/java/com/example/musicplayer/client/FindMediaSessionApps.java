package com.example.musicplayer.client;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * AsyncTask to get the list of apps with active media session.
 */
public class FindMediaSessionApps extends AsyncTask<Void, Void, ArrayList<MediaApp>> {
    
    /**
     * Implement this interface to get list of active MediaSession apps.
     */
    public interface DoneCallback {
        void FindSessionAppsDone( ArrayList<MediaApp> apps );
    }
    
    private DoneCallback m_callback;
    private WeakReference<Context> m_context;
    
    public FindMediaSessionApps( DoneCallback callback, Context context ) {
        m_callback = callback;
        m_context = new WeakReference<>( context );
    }
    
    @Override
    protected ArrayList<MediaApp> doInBackground( Void... voids ) {
        // Create new List for MediaApp objects. If no app implements MediaBrowserService return empty list.
        ArrayList<MediaApp> sessionAppDetails = new ArrayList<>();
        
        // Create empty List for MediaController objects to interact with an active media sessions.
        List<MediaController> controllers;
        
        try {
            
            // Get MediaSessionManager for managing media sessions.
            MediaSessionManager sessionManager = ( MediaSessionManager ) m_context.get().getSystemService( Context.MEDIA_SESSION_SERVICE );
            
            // Create ComponentName identifier using NotificationListenerService service.
            ComponentName listener = new ComponentName( m_context.get(), NotificationListener.class );
            
            // Get a list of controllers for all active media sessions.
            controllers = sessionManager.getActiveSessions( listener );
            
        } catch ( SecurityException e ) {
            
            // Exception will be thrown if user did not give app notification listener permission. In
            // that case just return empty List.
            return new ArrayList<>();
            
        }
        
        // Iterate through list of MediaController objects and add them to MediaApp list.
        for ( MediaController controller : controllers ) {
            
            // Holds the info about a particular application.
            ApplicationInfo info;
            
            try {
                
                // Get the info about current application.
                info = m_context.get().getPackageManager().getApplicationInfo( controller.getPackageName(), 0 );
                
            } catch ( PackageManager.NameNotFoundException e ) {
                
                // If an app with requested package name does not exist just continue to the next one.
                continue;
                
            }
            
            MediaApp app = new MediaApp( info, m_context.get().getPackageManager(), controller.getSessionToken() );
            
            sessionAppDetails.add( app );
        }
        
        return sessionAppDetails;
    }
    
    @Override
    protected void onPostExecute( ArrayList<MediaApp> controllers ) {
        // Pass the list of active MediaSession apps to class that implements DoneCallback interface.
        m_callback.FindSessionAppsDone( controllers );
    }
}
