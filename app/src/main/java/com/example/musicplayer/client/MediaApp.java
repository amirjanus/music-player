package com.example.musicplayer.client;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Drawable;
import android.media.session.MediaSession;
import android.support.v4.media.session.MediaSessionCompat;

import androidx.annotation.NonNull;

/**
 * Class holds details for apps that implement MediaBrowserService. It only has bare minimum of info
 * needed to connect our app to active media sessions.
 */
public class MediaApp {
    
    public String m_packageName;
    public String m_appName;
    public Drawable m_drawable;
    public MediaSessionCompat.Token m_sessionToken;
    
    /**
     * Constructor for MediaBrowserService apps.
     *
     * @param info    Class with info about MediaBrowserService service.
     * @param manager Class with info about installed apps.
     */
    public MediaApp( ServiceInfo info, PackageManager manager ) {
        m_packageName = info.packageName;
        m_appName = info.loadLabel( manager ).toString();
        m_drawable = info.loadIcon( manager );
        m_sessionToken = null;
    }
    
    /**
     * Constructor for MediaSession apps.
     *
     * @param info    Class with an information you about a particular application.
     * @param manager Class with info about installed apps.
     * @param token   Token used to create a MediaControllerCompat to interact with an active media session.
     */
    public MediaApp( ApplicationInfo info, PackageManager manager, MediaSession.Token token ) {
        m_packageName = info.packageName;
        m_appName = info.loadLabel( manager ).toString();
        m_drawable = info.loadIcon( manager );
        m_sessionToken = MediaSessionCompat.Token.fromToken( token );
    }
    
    /**
     * Used for printing class data to Logcat.
     */
    @NonNull
    @Override
    public String toString() {
        String s = "\nApp name: " + m_appName;
        s += "\nPackage name: " + m_packageName;
        s += "\nToken: " + m_sessionToken;
        
        return s;
    }
}
