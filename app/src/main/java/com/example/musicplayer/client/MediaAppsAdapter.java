package com.example.musicplayer.client;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.musicplayer.R;

import java.util.ArrayList;

/**
 * Adapter for RecyclerView to display list of media apps.
 */
public class MediaAppsAdapter extends RecyclerView.Adapter<MediaAppsAdapter.AppViewHolder> {
    
    /**
     * Implement this interface to get notified when user clicks Connect button in list item.
     */
    public interface OnItemSelectedListener {
        void OnItemSelected( MediaApp appDetails );
    }
    
    private OnItemSelectedListener m_listener;
    
    private ArrayList<MediaApp> m_apps;
    
    public MediaAppsAdapter( ArrayList<MediaApp> apps, OnItemSelectedListener listener ) {
        m_apps = apps;
        m_listener = listener;
    }
    
    @NonNull
    @Override
    public AppViewHolder onCreateViewHolder( @NonNull ViewGroup viewGroup, int viewType ) {
        View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.list_item_app, viewGroup, false );
        
        return new AppViewHolder( view );
    }
    
    @Override
    public void onBindViewHolder( @NonNull AppViewHolder appViewHolder, int position ) {
        final MediaApp current = m_apps.get( position );
        
        appViewHolder.m_appName.setText( current.m_appName );
        appViewHolder.m_appPackage.setText( current.m_packageName );
        appViewHolder.m_icon.setImageDrawable( current.m_drawable );
        appViewHolder.m_btnControl.setOnClickListener( v -> m_listener.OnItemSelected( current ) );
    }
    
    @Override
    public int getItemCount() {
        return m_apps.size();
    }
    
    class AppViewHolder extends RecyclerView.ViewHolder {
        
        public final TextView m_appName;
        public final TextView m_appPackage;
        public final ImageView m_icon;
        public final Button m_btnControl;
        
        public AppViewHolder( @NonNull View view ) {
            super( view );
            
            m_appName = view.findViewById( R.id.text_appName );
            m_appPackage = view.findViewById( R.id.text_packageName );
            m_icon = view.findViewById( R.id.image_icon );
            m_btnControl = view.findViewById( R.id.button_control );
        }
    }
}
