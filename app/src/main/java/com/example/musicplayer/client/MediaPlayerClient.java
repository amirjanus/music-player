package com.example.musicplayer.client;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

/**
 * Media player client contains media controller, and connects and communicates with a media browser
 * service.
 */
public class MediaPlayerClient extends Fragment implements FindMediaBrowserApps.DoneCallback,
                                                           FindMediaSessionApps.DoneCallback {
    
    /**
     * Interface for controller callbacks for receiving updates from the session.
     */
    public interface SessionCallbacks {
        void OnSessionReady( MediaControllerCompat.TransportControls playbackControls, MediaApp mediaApp );
        void OnSessionDestroyed();
        void OnMetadataChanged( MediaMetadataCompat metadata );
        void OnPlaybackStateChanged( PlaybackStateCompat playbackState );
    }
    
    /**
     * Implement this interface to get notified when AsyncTask ( FindBrowserApps ) fetches a list
     * of MediaBrowserService apps.
     */
    public interface BrowserAppsFoundListener {
        void MediaBrowserAppsFound( ArrayList<MediaApp> serviceApps );
    }
    
    /**
     * Implement this interface to get notified when AsyncTask ( FindSessionApps ) fetches a list
     * of apps with active media session. This interface is optional.
     */
    public interface SessionAppsFoundListener {
        void MediaSessionAppsFound( ArrayList<MediaApp> sessionApps );
    }
    
    // String key for identifying app package name in saved Bundle.
    private static String SELECTED_APP_KEY = "SELECTED_APP_KEY";
    // Tag for identifying MediaPlayerClient fragment in fragment stack.
    public static final String FRAGMENT_TAG = "MEDIA_PLAYER_CLIENT";
    
    // References to class that implement this class interfaces.
    private SessionCallbacks m_sessionCallbacks;
    private BrowserAppsFoundListener m_browserAppsListener;
    private SessionAppsFoundListener m_sessionAppsListener;
    
    private Context m_context;
    
    private MediaControllerCompat m_controller;
    private MediaControllerCompat.TransportControls m_playbackControls;
    
    // Package name of app that user selected to connected to.
    private String m_selectedApp;
    
    // List with apps that implement MediaBrowserService.
    private ArrayList<MediaApp> m_mediaServiceApps;
    // List with apps with active media session.
    private ArrayList<MediaApp> m_mediaSessionApps;
    
    // When both vars are true we will call AppsFoundListeners interface functions.
    private boolean m_areBrowserAppsFound;
    private boolean m_areSessionAppsFound;
    
    // Whether to update list of media browser and media session apps on onStart event.
    private boolean m_updateMediaAppsOnStart;
    
    public MediaPlayerClient() {
        m_mediaServiceApps = new ArrayList<>();
        m_mediaSessionApps = new ArrayList<>();
        
        m_updateMediaAppsOnStart = true;
    }
    
    /**
     * Initialize MediaPlayerClient and make sure only one instance of that class exists at all times.
     *
     * @param manager Fragment manager.
     */
    public static MediaPlayerClient GetInstance( FragmentManager manager ) {
        MediaPlayerClient mediaPlayerClient;
        
        // Check if fragment with tag FRAGMENT_TAG exists in fragment stack.
        if ( manager.findFragmentByTag( FRAGMENT_TAG ) == null ) {
        
            // Create new MediaPlayerClient fragment.
            mediaPlayerClient = new MediaPlayerClient();
        
            // Add MediaPlayerClient fragment to fragment stack.
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add( mediaPlayerClient, FRAGMENT_TAG );
            transaction.commit();
        
        } else {
        
            // MediaPlayerClient fragment already exists so get it from fragment manager.
            mediaPlayerClient = ( MediaPlayerClient ) manager.findFragmentByTag( FRAGMENT_TAG );
        
        }
        
        return mediaPlayerClient;
    }
    
    @Override
    public void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        
        // Check if we were connected to any app before configuration change happened.
        if ( savedInstanceState != null ) {
            m_selectedApp = savedInstanceState.getString( SELECTED_APP_KEY );
        }
        
        m_context = getContext();
    }
    
    @Override
    public void onSaveInstanceState( @NonNull Bundle outState ) {
        super.onSaveInstanceState( outState );
        
        if ( m_selectedApp != null ) {
            
            // Save the package name of the app to which we are connected.
            outState.putString( SELECTED_APP_KEY, m_selectedApp );
            
        }
    }
    
    @Override
    public void onStart() {
        super.onStart();
        
        if ( m_updateMediaAppsOnStart ) {
            
            // Update the lists with media browser and media session apps when user returns back to our app.
            FindBrowserApps();
            FindSessionApps();
            
        }
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        
        if ( m_controller != null ) {
            
            // Clean up playback controller.
            m_controller.unregisterCallback( m_controllerCallback );
            m_controller = null;
            
        }
    }
    
    /**
     * Set m_controllerCallback to receive event on media session change and when list of media browser and media
     * session apps are updated.
     *
     * @param sessionCallbacks         Callback to get notified on session creation and change.
     * @param browserAppsFoundListener Callback to get notified when media browser apps are found.
     * @param sessionAppsFoundListener Callback to get notified when media session apps are found.
     */
    public void SetCallbacks( SessionCallbacks sessionCallbacks, BrowserAppsFoundListener browserAppsFoundListener, SessionAppsFoundListener sessionAppsFoundListener ) {
        m_sessionCallbacks = sessionCallbacks;
        m_browserAppsListener = browserAppsFoundListener;
        m_sessionAppsListener = sessionAppsFoundListener;
    }
    
    /**
     * Set m_controllerCallback to receive event on media session change and when list of media browser apps are updated.
     *
     * @param sessionCallbacks         Callback to get notified on session creation and change.
     * @param browserAppsFoundListener Callback to get notified when media browser apps are found.
     */
    public void SetCallbacks( SessionCallbacks sessionCallbacks, BrowserAppsFoundListener browserAppsFoundListener ) {
        m_sessionCallbacks = sessionCallbacks;
        m_browserAppsListener = browserAppsFoundListener;
        m_sessionAppsListener = null;
    }
    
    /**
     * MediaPlayerClient updates list of media app on every onStart event by default. Pass false to this function to manually
     * update list.
     *
     * @param toUpdate True to auto update list, false to manually update.
     */
    public void SetUpdateOnStart( boolean toUpdate ) {
        m_updateMediaAppsOnStart = toUpdate;
    }
    
    /**
     * Check if user has given this app permission to access notifications.
     *
     * @param context App context.
     * @return True if app has notification listener permission, false otherwise.
     */
    public static boolean IsNotificationListenerEnabled( Context context ) {
        return NotificationListener.IsEnabled( context );
    }
    
    /**
     * Open settings window for user to enable notification listener permission. This is needed to
     * get apps with active media session.
     */
    public static void RequestNotificationAccess( Context context ) {
        context.startActivity( new Intent( "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS" ) );
    }
    
    /**
     * Start AsyncTask to fetch list of apps that implement MediaBrowserService.
     */
    public void FindBrowserApps() {
        m_areBrowserAppsFound = false;
        
        new FindMediaBrowserApps( this, m_context ).execute();
    }
    
    /**
     * Called after AsyncTask finds apps that implement MediaBrowserService.
     *
     * @param serviceApps List of apps that implement MediaBrowserService.
     */
    @Override
    public void FindBrowserAppsDone( ArrayList<MediaApp> serviceApps ) {
        m_mediaServiceApps = serviceApps;
        
        m_areBrowserAppsFound = true;
        
        MediaAppsFound();
    }
    
    /**
     * Start AsyncTask to fetch list of apps with active MediaSession.
     */
    public void FindSessionApps() {
        m_areSessionAppsFound = false;
        
        new FindMediaSessionApps( this, m_context ).execute();
    }
    
    /**
     * Called after AsyncTask finds apps with active MediaSession.
     *
     * @param sessionApps List of apps with active MediaSession.
     */
    @Override
    public void FindSessionAppsDone( ArrayList<MediaApp> sessionApps ) {
        m_mediaSessionApps = sessionApps;
        
        m_areSessionAppsFound = true;
        
        MediaAppsFound();
    }
    
    /**
     * We need to wait for AsyncTasks to get both the list with media browser apps and media session
     * apps. That is necessary to avoid case when browser app is selected before AsyncTask got media
     * session apps.
     */
    private void MediaAppsFound() {
        // Check if both browser and session apps are found.
        if ( m_areBrowserAppsFound && m_areSessionAppsFound ) {
            
            ReconnectToSession();
            
            m_browserAppsListener.MediaBrowserAppsFound( m_mediaServiceApps );
            
            if ( m_sessionAppsListener != null ) {
                m_sessionAppsListener.MediaSessionAppsFound( m_mediaSessionApps );
            }
            
        }
    }
    
    /**
     * When user is returning to app or after configuration change connect to app whose package
     * name was saved.
     */
    private void ReconnectToSession() {
        // If user has not selected any app there is nothing to connect to.
        if ( m_selectedApp == null ) {
            return;
        }
        
        // Find selected app in media session apps list.
        MediaApp mediaApp = GetSessionAppByPackageName( m_selectedApp );
        
        if ( mediaApp != null ) {
            
            // We have found requested app so just connect to it.
            ConnectToSession( mediaApp );
            
        } else {
            
            // App that the user selected no longer has active media session so remove its package name.
            m_selectedApp = null;
            
        }
    }
    
    /**
     * Get app that matches requested package name from the list of media session apps.
     *
     * @param packageName Package name of requested app.
     * @return Reference to MediaApp object or null if no such object.
     */
    private MediaApp GetSessionAppByPackageName( String packageName ) {
        for ( MediaApp mediaApp : m_mediaSessionApps ) {
            
            if ( mediaApp.m_packageName.equals( packageName ) ) {
                return mediaApp;
            }
            
        }
        
        return null;
    }
    
    /**
     * When user selected an app we will connect to it or launch it depending whether that app has
     * active media session or not.
     *
     * @param app App to connect to.
     */
    public void ConnectToApp( MediaApp app ) {
        m_selectedApp = app.m_packageName;
        
        // Find selected app in media session apps list.
        MediaApp mediaApp = GetSessionAppByPackageName( m_selectedApp );
        
        if ( mediaApp != null ) {
            
            // We have found selected app so just connect with it.
            ConnectToSession( mediaApp );
            
        } else {
            
            // App that the user selected does not have active media session so launch it to create one.
            LaunchApp( app );
            
        }
    }
    
    /**
     * Launch app after user has selected to start its media session.
     *
     * @param appDetails App to launch.
     */
    public void LaunchApp( MediaApp appDetails ) {
        Intent launchIntent = m_context.getPackageManager().getLaunchIntentForPackage( appDetails.m_packageName );
        
        m_context.startActivity( launchIntent );
    }
    
    /**
     * Connect to requested app active media session.
     *
     * @param mediaApp App to connect to.
     */
    public void ConnectToSession( MediaApp mediaApp ) {
        // Do not connect again to app which we are already connected to.
        if ( m_controller != null && mediaApp.m_packageName.equals( m_controller.getPackageName() ) ) {
            return;
        }
        
        try {
            
            // Create new media controller.
            MediaControllerCompat controller = new MediaControllerCompat( m_context, mediaApp.m_sessionToken );
            
            // Unregister callback from the old media controller if it exists.
            if ( m_controller != null ) {
                m_controller.unregisterCallback( m_controllerCallback );
            }
            
            // Save the reference to new controller and attach a callback to it to receive updates from the session.
            m_controller = controller;
            m_controller.registerCallback( m_controllerCallback );
            
        } catch ( RemoteException e ) {
            
            m_selectedApp = null;
            
            Log.d( "MediaPlayerClient", "Failed to connect to: " + mediaApp.m_packageName, e );
            
        }
    }
    
    /**
     * This callback is registered with MediaController so it can receive updates from the session.
     */
    MediaControllerCompat.Callback m_controllerCallback = new MediaControllerCompat.Callback() {
        /**
         * Called when playback state changes ( play, pause, ... ).
         *
         * @param playbackState Playback state.
         */
        @Override
        public void onPlaybackStateChanged( PlaybackStateCompat playbackState ) {
            if ( playbackState != null ) {
                m_sessionCallbacks.OnPlaybackStateChanged( playbackState );
            }
        }
        
        /**
         * Called when session is being ready.
         */
        @Override
        public void onSessionReady() {
            // Get class for controlling media playback on a session:
            m_playbackControls = m_controller.getTransportControls();
            
            // Get the app that we are currently connected to.
            MediaApp connectedApp = GetSessionAppByPackageName( m_selectedApp );
            
            // Notify caller that media session is ready.
            m_sessionCallbacks.OnSessionReady( m_playbackControls, connectedApp );
            
            // Force a call to onMetadataChanged callback to get metadata.
            onMetadataChanged( m_controller.getMetadata() );
            
            // Force a call to onPlaybackStateChanged to get playback state.
            onPlaybackStateChanged( m_controller.getPlaybackState() );
        }
        
        /**
         * Called when session is being destroyed.
         */
        @Override
        public void onSessionDestroyed() {
            m_playbackControls = null;
            
            m_controller = null;
            
            m_selectedApp = null;
            
            m_sessionCallbacks.OnSessionDestroyed();
        }
        
        /**
         * Called when track metadata changes.
         *
         * @param metadata Contains metadata about a track.
         */
        @Override
        public void onMetadataChanged( MediaMetadataCompat metadata ) {
            if ( metadata != null ) {
                m_sessionCallbacks.OnMetadataChanged( metadata );
            }
        }
    };
}
