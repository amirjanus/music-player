package com.example.musicplayer.client;

import android.content.Context;
import android.service.notification.NotificationListenerService;

import androidx.core.app.NotificationManagerCompat;

import java.util.Set;

/**
 * Service that enables us to query notifications. We need this class to get active media session
 * apps from notifications.
 */
public class NotificationListener extends NotificationListenerService {
    
    /**
     * Check if user has given this app permission to access notifications.
     *
     * @param context App context.
     * @return True if this app has notification access, false otherwise.
     */
    public static boolean IsEnabled( Context context ) {
        // Get a Set of of app package names that have notification access.
        Set<String> listenerApps = NotificationManagerCompat.getEnabledListenerPackages( context );
        
        // Checks if this app is in that Set.
        return listenerApps.contains( context.getPackageName() );
    }
}
