package com.example.musicplayer.client;

import android.support.v4.media.session.PlaybackStateCompat;

import java.util.TreeMap;

/**
 * Helper class for PlaybackStateCompat class.
 */
public class PlaybackStateHelper {
    // Map with PlaybackState actions.
    public static TreeMap<String, Long> m_action = new TreeMap<>();
    
    // Execute this block once to populate map of PlaybackState actions.
    static {
        m_action.put( "ACTION_FAST_FORWARD", PlaybackStateCompat.ACTION_FAST_FORWARD );
        m_action.put( "ACTION_PAUSE", PlaybackStateCompat.ACTION_PAUSE );
        m_action.put( "ACTION_PLAY", PlaybackStateCompat.ACTION_PLAY );
        m_action.put( "ACTION_REWIND", PlaybackStateCompat.ACTION_REWIND );
        m_action.put( "ACTION_SET_REPEAT_MODE", PlaybackStateCompat.ACTION_SET_REPEAT_MODE );
        m_action.put( "ACTION_SET_SHUFFLE_MODE", PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE );
        m_action.put( "ACTION_SKIP_TO_NEXT", PlaybackStateCompat.ACTION_SKIP_TO_NEXT );
        m_action.put( "ACTION_SKIP_TO_PREVIOUS", PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS );
        m_action.put( "ACTION_STOP", PlaybackStateCompat.ACTION_STOP );
    }
    
    /**
     * Check if particular playback action is supported.
     *
     * @param playbackState Playback state.
     * @param action        Action to check.
     * @return True if action is supported, false otherwise.
     */
    public static boolean IsActionSupported( PlaybackStateCompat playbackState, long action ) {
        long actions = playbackState.getActions();
        
        return ( ( actions & action ) != 0 );
    }
    
    /**
     * Returns shuffle mode value to set MediaController shuffle mode.
     *
     * @param shuffleMode String from string array "shuffle_mode" in strings.xml.
     * @return Shuffle mode value.
     */
    public static int GetShuffleModeValue( String shuffleMode ) {
        switch ( shuffleMode ) {
            case "None":
                return PlaybackStateCompat.SHUFFLE_MODE_NONE;
            
            case "All":
                return PlaybackStateCompat.SHUFFLE_MODE_ALL;
            
            case "Group":
                return PlaybackStateCompat.SHUFFLE_MODE_GROUP;
            
            default:
                return -1;
        }
    }
    
    /**
     * Returns repeat mode value to set MediaController repeat mode.
     *
     * @param repeatMode String from string array "repeat_mode" in strings.xml.
     * @return Repeat mode value.
     */
    public static int GetRepeatModeValue( String repeatMode ) {
        switch ( repeatMode ) {
            case "None":
                return PlaybackStateCompat.REPEAT_MODE_NONE;
            
            case "One":
                return PlaybackStateCompat.REPEAT_MODE_ONE;
            
            case "All":
                return PlaybackStateCompat.REPEAT_MODE_ALL;
            
            case "Group":
                return PlaybackStateCompat.REPEAT_MODE_GROUP;
            
            default:
                return -1;
        }
    }
}
